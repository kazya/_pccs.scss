#PCCSとは
PCCSは[日本色研](http://www.sikiken.co.jp/pccs/)が1964年に発表したカラーシステムです。

色をトーンごとに分類し、各トーン内の色相が同じになるように番号をつけ、配色をシステマティックに解決することを主な目的として開発されました。

配色技法として、このPCCSトーンを利用した「単一色相（モノトーン）配色」や「同一トーン（トーン・オン・トーン）配色」などがあります。

_pccs.scssのRGB値は[GARAKUTA.net](http://www.garakuta.net/color/pccs/matrix.html)様にて公開されている設定値を使用し、[姫路のホームページ製作屋 WILDWEST-SERVICE](http://www.wildwest-service.com)が製作しました。

#CSSカラーパレットの使い方
_pccs.scssファイルをSass（Scss）用フォルダに入れ、次の記述をお使いのSass（Scss）ファイルに追加してください。

```
	@import "_pccs.scss";
```

Sass（Scss）ファイル内で、RGB値の色番号を記述する部分を次のように書いてください。<br>[tone]はトーン名、[color-number]は色番号に書き換えて使用します。

```
	nth([tone], [color-number])
```
	
たとえば、Vivid-toneの中からv1の色をbackground-colorとして使いたい場合には、次のように記述します。

```
	div {
	   background-color: nth($v, 1);
	}
```

一般的なPCCS色見本に基づき、Vividは1～24、その他の色は2～24の偶数のみを用意しています。

色見本は本データセット内のreadme.htmlをブラウザで開いてご覧いただけます。

## PhotoshopとIllustratorのスウォッチ

PhotoshopとIllustrator用にスウォッチデータを同梱しています。

CSSカラーパレットと同じ色名をつけていますので、デザインデータ作成時にお使いいただけます。

+ PCCS_Illustrator.ai - Illustrator用スウォッチ
+ PCCS_photoshop.aco - Photoshop用スウォッチ
+ PCCS_global.ase - 交換用スウォッチ（Fireworks、InDesignなど）

スウォッチデータの読み込み方法については各アプリケーションのマニュアルをご参照ください。